/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cburch.autosim;

import java.util.ArrayList;

/**
 *
 * @author john
 */
public class NewTape implements Tape {
    private ArrayList<Character> left=new ArrayList<Character>();
    private ArrayList<Character> right=new ArrayList<Character>();
    private ArrayList<TapeListener> tl=new  ArrayList<>();
    int currPos=0;
    int head=0;
        @Override
    public void addTapeListener(TapeListener listener) {
    tl.add(listener);    
    }

    @Override
    public void completeReset() {
        left=new ArrayList<>();
        right=new ArrayList<>();
        
    }

    @Override
    public int getCursorPosition() {
    return currPos;
    }

    @Override
    public int getHeadPosition() {
    return head;
    }

    @Override
    public char read(int pos) {
    int movement =this.currPos-pos;
        if(movement>0){
            return right.get(movement);
        }
        else{
            return left.get(movement);
        }
    }

    @Override
    public void removeTapeListener(TapeListener listener) {
    for( TapeListener ls : tl){
        if(ls.equals(listener))
            tl.remove(ls);
    }   
    }

    @Override
    public void reset() {
    
    }

    @Override
    public void setCursorPosition(int value) {
    for(int i=0; i< currPos-value;i++){
        
    }
        
    }

    @Override
    public void setExtendsLeft(boolean value) {
    
    }

    @Override
    public void setHeadPosition(int value) {
    
    }

    @Override
    public void setShowHead(boolean value) {
    //nicht benoetigt
    }

    @Override
    public void write(int pos, char value) {
    if(currPos>pos)
        left.set(Math.abs(currPos-pos), value);
    else
        right.set(Math.abs(currPos-pos),value);
    }
    
}
