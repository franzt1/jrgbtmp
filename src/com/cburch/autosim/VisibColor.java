/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cburch.autosim;

import java.awt.Color;

/**
 *
 * @author john
 */
public class VisibColor {

    static Color toColor(char a){
        if (a=='1') return Color.red;
        if (a=='2') return Color.yellow;
        if (a=='3') return Color.blue;
        if (a=='4') return Color.green;
        if (a=='5') return Color.pink;
        if (a=='6') return Color.orange;
        if (a=='7') return Color.white;
        if (a=='_')return Color.black;
        else return Color.gray;

    }
    
}
