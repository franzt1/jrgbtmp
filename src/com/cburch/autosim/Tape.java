/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cburch.autosim;

/**
 *
 * @author john
 */
public interface Tape {

    // instance methods
    void addTapeListener(TapeListener listener);

    void completeReset();

    int getCursorPosition();

    int getHeadPosition();

    char read(int pos);

    void removeTapeListener(TapeListener listener);

    void reset();

    void setCursorPosition(int value);

    void setExtendsLeft(boolean value);

    void setHeadPosition(int value);

    /*
    //    public Animation setHeadPositionAnimate(int value) {
    //        representation.expose(head);
    //        int old_head = head;
    //        head = value;
    //        representation.expose(head);
    //        moveIntoView(head);
    //        representation.computeSize();
    //        return new HeadAnimation(old_head, value);
    //    }
     */
    void setShowHead(boolean value);

    void write(int pos, char value);
    
}
